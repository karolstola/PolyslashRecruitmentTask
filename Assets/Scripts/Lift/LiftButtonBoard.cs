﻿using UnityEngine;
using System.Collections.Generic;

public class LiftButtonBoard : MonoBehaviour
{
	[SerializeField]
	private GameObject buttonsHolder;

	[SerializeField]
	private GameObject buttonPrefab;

	public event LiftCage.GoToFloorRequest RequestGoToFloor;

	private int buttonCount;
	private List<LiftButton> liftButtons = new List<LiftButton>();

	public int ButtonCount
	{
		get
		{
			return liftButtons.Count;
		}
		set
		{
			SetButtonCount(value);
		}
	}

	private void SetButtonCount(int count)
	{
		if(count > ButtonCount)
		{
			IncreaseButtonCountTo(count);
		}
		else if (count < ButtonCount)
		{
			DecreaseButtonCountTo(count);
		}
	}

	private void IncreaseButtonCountTo(int count)
	{
		while(ButtonCount < count)
		{
			CreateNewButton ();
		}
	}

	void CreateNewButton ()
	{
		var button = GameObject.Instantiate (buttonPrefab);

		var buttonComponent = button.GetComponent<LiftButton> ();
		buttonComponent.FloorNumber = ButtonCount;
		buttonComponent.ClickCallback += ClickCallback;

		button.transform.SetParent (buttonsHolder.transform, false);
		button.transform.SetSiblingIndex(0);
		liftButtons.Add(buttonComponent);
	}

	private void DecreaseButtonCountTo(int count)
	{
		while(ButtonCount > count)
		{
			var lastButtonIndex = liftButtons.Count-1;
			GameObject.Destroy(liftButtons[lastButtonIndex].gameObject);
			liftButtons.RemoveAt(lastButtonIndex);
		}
	}

	public void OnArrivalOnFloor(int floor)
	{
		liftButtons[floor].OnButtonsFloorArrival();
	}

	private void ClickCallback(LiftButton button)
	{
		RequestGoToFloor(button.FloorNumber);
	}

	public void SetCurrentFloor(int floor)
	{
		foreach (var button in liftButtons)
		{
			button.SetCurrentFloorHighlight(floor == button.FloorNumber);
		}
	}
}
