﻿using UnityEngine;
using System.Collections;

public class LiftShaft : MonoBehaviour
{
	[SerializeField]
	private LiftShaftWalls shaftWalls;

	private float floorHeight = 1f;

	private int floorCount = 1;

	public float FloorHeight
	{
		set
		{
			floorHeight = value;
			SetShaftHeight(ShaftHeight);
		}
	}

	public int FloorCount
	{
		set
		{
			floorCount = value;
			SetShaftHeight(ShaftHeight);
		}
	}

	private float ShaftHeight
	{
		get
		{
			return floorCount * floorHeight;
		}
	}

	private void SetShaftHeight(float height)
	{
		var scale = shaftWalls.transform.localScale;
		scale.y = height;
		shaftWalls.transform.localScale = scale;
	}

}
