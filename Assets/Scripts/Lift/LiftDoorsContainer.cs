﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LiftDoorsContainer : MonoBehaviour
{
	[SerializeField]
	private GameObject doorPrefab;

	[SerializeField]
	private bool CreateDebugFloors;

	[SerializeField]
	private GameObject floorPrefab;

	public delegate void DoorClosedOnFloorNotification(int floor);
	public delegate void DoorButtonClickedOnFloor(int floor);

	public int FloorCount { get; set; }
	public float FloorHeight { get; set; }
	public int CageCurrentFloor { get; set; }

	public event DoorClosedOnFloorNotification NotifyDoorClosedOnFloor;
	public event DoorButtonClickedOnFloor NotifyDoorButtonClickedOnFloor;

	private List<LiftDoor> doors = new List<LiftDoor>();


	private void Start()
	{
		for(int floor = 0; floor < FloorCount; floor++)
		{
			CreateDoor (floor);
			if(CreateDebugFloors)
			{
				CreateFloor (floor);
			}
		}
	}

	private void CreateDoor (int floor)
	{
		var door = GameObject.Instantiate (doorPrefab).GetComponent<LiftDoor> ();
		doors.Add (door);
		door.Floor = floor;
		door.CanOpen = floor == CageCurrentFloor;
		door.NotifyDoorsClosed += OnDoorsClosed;
		door.NotifyButtonClicked += OnDoorButtonClicked;
		door.transform.SetParent (this.transform);
		door.transform.localPosition = new Vector3 (0, floor * FloorHeight, 0);
	}

	private void CreateFloor(int floor)
	{
		var floorObject = GameObject.Instantiate(floorPrefab);
		floorObject.transform.SetParent(doors[floor].transform);
		floorObject.transform.localPosition = new Vector3(0,0,floorObject.transform.localScale.z/-2);
	}

	public void OnArrivalOnFloor(int floor)
	{
		CageCurrentFloor = floor;
		doors[floor].CanOpen = true;
		doors[floor].OpenIfPossible();
	}

	public void OnCloseDoorRequested(int floor)
	{
		doors[floor].CloseIfPossible();
	}

	private void OnDoorsClosed(LiftDoor closedDoors)
	{
		NotifyDoorClosedOnFloor(closedDoors.Floor);
	}

	private void OnDoorButtonClicked(LiftDoor door)
	{
		if(door.Floor == CageCurrentFloor)
		{
			door.OpenIfPossible();
		}
		NotifyDoorButtonClickedOnFloor(door.Floor);
	}

	public void OnCageStartedMoving()
	{
		doors[CageCurrentFloor].CanOpen = false;
	}
}
