﻿using UnityEngine;
using System.Collections.Generic;

public class LiftCage : MonoBehaviour
{
	[SerializeField]
	[Range(0f, 10f)]
	private float speed;

	[SerializeField]
	private LiftButtonBoard buttonBoard;

	[SerializeField]
	private AudioSource speaker;

	public  bool IsMoving { get; private set;}

	public delegate void GoToFloorRequest(int floor);
	public delegate void ArrivedOnFloorNotification(int floor);
	public delegate void CloseDoorRequest(int floor);
	public delegate void StartedMovingNotification();

	public event ArrivedOnFloorNotification NotifyFloorArrival;
	public event CloseDoorRequest RequestCloseDoor;
	public event StartedMovingNotification NotifyStartedMoving;

	private int floorCount;
	private List<int> floorsToVisit = new List<int>();

	private const float ACCEPTABLE_FLOOR_LEVEL_OFFSET = 0.05f;
	private const string SPEED_TOO_HIGH_WARNING_TEXT = "Current speed of {0} is too high for acceptable floor level offset. ";

	public int FloorCount
	{
		get { return floorCount; }
		set
		{
			floorCount = value;
			buttonBoard.ButtonCount = value;
		}
	}

	public bool IsAtFloorLevel
	{
		get { return Position % FloorHeight < ACCEPTABLE_FLOOR_LEVEL_OFFSET; }
	}

	public int CurrentFloor
	{
		get { return GetFloorForPosition(Position); }
	}

	public float FloorHeight { get; set; }

	private float Position
	{
		get { return transform.localPosition.y; }
	}

	private float CurrentFrameMoveDelta
	{
		get
		{
			var distance = speed * Time.fixedDeltaTime;
			var destination = GetFloorPosition (CurrentFloorDestination);
			var direction = Mathf.Sign ( destination - Position );
			if(distance > ACCEPTABLE_FLOOR_LEVEL_OFFSET)
			{
				Debug.LogWarning(string.Format(SPEED_TOO_HIGH_WARNING_TEXT, distance));
			}
			return distance * direction;
		}
	}

	private int CurrentFloorDestination
	{
		get
		{
			return floorsToVisit[0];
		}
	}

	void Start()
	{
		buttonBoard.RequestGoToFloor += OnGoToFloorRequested;
		NotifyFloorArrival += buttonBoard.OnArrivalOnFloor;
	}

	private void FixedUpdate()
	{
		if(IsMoving)
		{
			ManageMovement ();
		}
	}

	private void Update()
	{
		buttonBoard.SetCurrentFloor(CurrentFloor);
	}

	public void StartMoving()
	{
		speaker.Play();
		IsMoving = true;
		NotifyStartedMoving();
	}

	private void ManageMovement ()
	{
		if (IsAtFloorLevel && ShouldGoToFloor (CurrentFloor))
		{
			Stop();
		}
		else
		{
			MoveBy (CurrentFrameMoveDelta);
		}
	}

	private void Stop()
	{
		IsMoving = false;
		floorsToVisit.RemoveAll((x)=>
		{
			return x == CurrentFloor;
		});
		speaker.Stop();
		NotifyFloorArrival(CurrentFloor);
	}

	private float GetFloorPosition(int floor)
	{
		return floor * FloorHeight;
	}

	private int GetFloorForPosition(float position)
	{
		return (int) ( position / FloorHeight );
	}

	private bool IsAtFloor(int floor)
	{
		return floor == GetFloorForPosition(floor);
	}

	private bool ShouldGoToFloor(int floor)
	{
		return floorsToVisit.Contains(floor);
	}

	private void MoveBy(float move)
	{
		var position = transform.localPosition;
		position.y += move;
		transform.localPosition = position;
	}

	public void OnGoToFloorRequested(int floor)
	{
		if(IsMoving || floor != CurrentFloor)
		{
			floorsToVisit.Add(floor);
		}

		if(floor != CurrentFloor)
		{
			RequestCloseDoor(CurrentFloor);
		}
	}

	public void OnDoorsClosedOnFloor(int floor)
	{
		if(!IsMoving && floor == CurrentFloor && floorsToVisit.Count != 0)
		{
			StartMoving();
		}
	}
}
