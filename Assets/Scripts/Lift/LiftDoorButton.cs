﻿using UnityEngine;
using UnityEngine.UI;

public class LiftDoorButton : MonoBehaviour
{
	[SerializeField]
	private Color baseColor = Color.white;

	[SerializeField]
	private Color openColor = Color.green;

	[SerializeField]
	private Text label;

	public delegate void ClickCallback();
	public event ClickCallback OnClickCallback;

	private Button button;
	private Image image;
	private bool isWaiting = false;

	private const string IDLE_LABEL = "call lift";
	private const string WAITING_LABEL = "please wait...";
	private const string ARRIVED_LABEL = "please step in";

	private void Awake()
	{
		button = GetComponent<Button>();
		image = GetComponent<Image>();
		image.color = baseColor;
	}

	public void OnClick()
	{
		image.color = baseColor;
		isWaiting = true;
		label.text = WAITING_LABEL;
		button.interactable = false;
		OnClickCallback();
	}

	public void OnDoorOpening()
	{
		image.color = openColor;
		isWaiting = false;
		label.text = ARRIVED_LABEL;
		button.interactable = false;
	}

	public void OnDoorClosed()
	{
		if(!isWaiting)
		{
			image.color = baseColor;
			label.text = IDLE_LABEL;
			button.interactable = true;
		}
	}
}
