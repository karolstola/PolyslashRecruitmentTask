﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Lift))]
public class LiftEditor : Editor
{
	private Lift lift;

	private SerializedProperty liftCage;
	private SerializedProperty liftShaft;
	private SerializedProperty floorHeight;
	private SerializedProperty floorCount;
	private SerializedProperty liftDoors;

	void OnEnable()
	{
		lift = (Lift) target;

		liftCage = serializedObject.FindProperty("liftCage");
		liftShaft = serializedObject.FindProperty("liftShaft");
		floorHeight = serializedObject.FindProperty("floorHeight");
		floorCount = serializedObject.FindProperty("floorCount");
		liftDoors = serializedObject.FindProperty("liftDoors");
	}

	public override void OnInspectorGUI()
	{
		EditorGUILayout.PropertyField(floorHeight);
		EditorGUILayout.PropertyField(floorCount);
		EditorGUILayout.PropertyField(liftCage);
		EditorGUILayout.PropertyField(liftShaft);
		EditorGUILayout.PropertyField(liftDoors);
		serializedObject.ApplyModifiedProperties();
		lift.UpdateFloorHeight();
		lift.UpdateFloorCount();
	}
}
