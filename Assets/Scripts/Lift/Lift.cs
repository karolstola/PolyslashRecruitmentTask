﻿using UnityEngine;
using System.Collections;

public class Lift : MonoBehaviour
{
	[SerializeField]
	[Range(1f, 10f)]
	private float floorHeight = 1f;

	[SerializeField]
	[Range(1,100)]
	private int floorCount = 1;

	[SerializeField]
	private LiftCage liftCage;

	[SerializeField]
	private LiftShaft liftShaft;

	[SerializeField]
	private LiftDoorsContainer liftDoors;

	void Awake()
	{
		liftCage.FloorCount = floorCount;
		liftCage.FloorHeight = floorHeight;
		liftCage.NotifyFloorArrival += liftDoors.OnArrivalOnFloor;
		liftCage.RequestCloseDoor += liftDoors.OnCloseDoorRequested;
		liftCage.NotifyStartedMoving += liftDoors.OnCageStartedMoving;

		liftDoors.FloorCount = floorCount;
		liftDoors.FloorHeight = floorHeight;
		liftDoors.CageCurrentFloor = liftCage.IsAtFloorLevel ? liftCage.CurrentFloor : -1;
		liftDoors.NotifyDoorClosedOnFloor += liftCage.OnDoorsClosedOnFloor;
		liftDoors.NotifyDoorButtonClickedOnFloor += liftCage.OnGoToFloorRequested;
	}

	public void UpdateFloorHeight()
	{
		liftShaft.FloorHeight = floorHeight;
	}

	public void UpdateFloorCount()
	{
		liftShaft.FloorCount = floorCount;
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		for(int i = 0; i < floorCount; i++)
		{
			DrawFloorLevelIndicator (i);
		}
	}

	void DrawFloorLevelIndicator (int floor)
	{
		var position = new Vector3 (0, floor * floorHeight, 0);
		position += transform.position;
		var size = new Vector3 (5f, 0.1f, 5f);
		Gizmos.DrawWireCube (position, size);
	}
}
