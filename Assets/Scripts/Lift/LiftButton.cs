﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LiftButton : MonoBehaviour
{
	[SerializeField]
	private Color standardColor = Color.white;

	[SerializeField]
	private Color currentFloorHighlightColor = Color.green;

	[SerializeField]
	private Text label;

	private int floorNumber;
	private Button button;
	private Image buttonImage;

	public delegate void ButtonClickedCallback(LiftButton button);
	public event ButtonClickedCallback ClickCallback;

	private void Awake()
	{
		button = GetComponent<Button>();
		buttonImage = GetComponent<Image>();
	}

	public int FloorNumber
	{
		get
		{
			return floorNumber;
		}
		set
		{
			floorNumber = value;
			label.text = value.ToString();
		}
	}

	public void SetCurrentFloorHighlight(bool isCurrentFloorButton)
	{
		buttonImage.color = isCurrentFloorButton ? currentFloorHighlightColor : standardColor;
	}

	public void OnButtonsFloorArrival()
	{
		button.interactable = true;
	}

	public void OnClicked()
	{
		button.interactable = false;
		ClickCallback(this);
	}
}

