﻿using UnityEngine;
using System.Collections;

public class LiftDoor : MonoBehaviour
{
	[SerializeField]
	private float closeDelay;

	[SerializeField]
	private LiftDoorButton button;

	[SerializeField]
	private AudioClip openingBell;

	[SerializeField]
	private AudioClip closingBell;

	[SerializeField]
	private AudioSource doorSpeaker;

	public bool AreClosed { get; private set; }
	public bool AreOpen { get; private set; }
	public bool CanOpen { get; set; }

	public int Floor{ get; set; }
	public delegate void DoorClosedNotification(LiftDoor closedDoor);
	public delegate void DoorButtonClickedNotification(LiftDoor door);

	public event DoorClosedNotification NotifyDoorsClosed;
	public event DoorButtonClickedNotification NotifyButtonClicked;

	private Animator animator;
	private int objectsBlocking = 0;

	private const string ANIMATOR_OPEN_FLAG_NAME = "Open";

	private void Awake()
	{
		button.OnClickCallback += OnButtonClicked;
		animator = GetComponent<Animator>();
		AreClosed = true;
		AreOpen = false;
	}

	private void OnButtonClicked()
	{
		NotifyButtonClicked(this);
	}

	public void OpenIfPossible()
	{
		StopAllCoroutines ();
		if(CanOpen)
		{
			Open ();
		}
	}

	private void Open ()
	{
		if (AreOpen)
		{
			OnDoorsOpen ();
		}
		else
		{
			StartOpening ();
		}
	}

	public void OnDoorsOpen()
	{
		AreClosed = false;
		AreOpen = true;
		DelayClosingIfPossible ();
	}

	void StartOpening ()
	{
		AreOpen = false;
		AreClosed = false;
		button.OnDoorOpening ();
		doorSpeaker.PlayOneShot(openingBell);
		animator.SetBool (ANIMATOR_OPEN_FLAG_NAME, true);
	}

	public void CloseIfPossible()
	{
		StopAllCoroutines();
		if(AreClosed)
		{
			OnDoorsClosed();
		}
		else if (AreOpen && objectsBlocking < 1)
		{
			StartClosing ();
		}
	}

	public void OnDoorsClosed()
	{
		AreClosed = true;
		AreOpen = false;
		button.OnDoorClosed();
		NotifyDoorsClosed(this);
	}

	void StartClosing ()
	{
		AreOpen = false;
		AreClosed = false;
		doorSpeaker.PlayOneShot(closingBell);
		animator.SetBool (ANIMATOR_OPEN_FLAG_NAME, false);
	}

	void DelayClosingIfPossible ()
	{
		if (objectsBlocking <= 0)
		{
			StartCoroutine (DelayClosingCoroutine ());
		}
	}

	public IEnumerator DelayClosingCoroutine()
	{
		yield return new WaitForSeconds(closeDelay);
		CloseIfPossible();
	}

	private void OnTriggerEnter(Collider collider)
	{
		if(ShouldAffectDoor(collider))
		{
			objectsBlocking++;
			OpenIfPossible();
		}
	}

	private void OnTriggerExit(Collider collider)
	{
		if(ShouldAffectDoor(collider))
		{
			objectsBlocking--;
			DelayClosingIfPossible();
		}
	}

	private bool ShouldAffectDoor(Collider collider)
	{
		return collider.GetComponent<CharacterController>() != null;
	}
}
