﻿using UnityEngine;
using System.Collections;

public class CharacterLiftRideSmoothener : MonoBehaviour
{
	private Transform originalParent;

	void Awake ()
	{
		originalParent = transform.parent;
	}
	
	void OnTriggerEnter(Collider collider)
	{
		if(collider.GetComponent<LiftCage>() != null)
		{
			transform.SetParent(collider.transform);
		}
	}

	void OnTriggerExit(Collider collider)
	{
		if(collider.GetComponent<LiftCage>() != null)
		{
			transform.SetParent(originalParent);
		}
	}
}
